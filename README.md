git-word-blame
-------

Show word-by-word authors of a file

## Demo

Executing `git word-blame git.c` on the `git` repository we get:

* [word-blame-by-commit.html](https://dam.io/word-blame/git/word-blame-by-commit.html)
* [word-blame-by-author.html](https://dam.io/word-blame/git/word-blame-by-author.html)
* [authors_stats.tsv](https://dam.io/word-blame/git/author_stats.tsv)
* [commit_stats.tsv](https://dam.io/word-blame/git/commit_stats.tsv)

<!--
[![screenshot](link-to-screenshot)](link_to_demo.html)
-->

## Installation

```
pip install git-word-blame
```


## Usage

```
git word-blame <path/to/my/file>
```

It will produce the following files (by default in `/tmp/git-word-blame/`):

```
- author_stats.tsv         # top authors by number of characters attributed to them
- commit_stats.tsv          # same for commits
- word-blame-by-commit.html # hover on some text to see which commit created it
- word-blame-by-author.html # same for authors
- text-output # tokens grouped by authors as a grep-friendly output
```


## Authorship algorithms

Two algorithms are available:

  - `wikiwho` (**default**): coarse but more robust
  - `mwpersistence`: precise but with more false-positives

To change the engine used by `git word-blame`:

```
git config --global word-blame.engine mwpersistence
```


## HTML output

You can choose between 3 themes for the HTML output: `black-and-white`, `solarized-dark`, `solarized-light`.

```
git config --global word-blame.theme solarized-dark
```

<!-- screenshots/themes.png -->

By default a git command is shown when clicking on the text, you can change this behavior and make it go to the diff online (Github/Framagit) directly:

```
git config --global --bool word-blame.link-to-online-commit true
```


## Limits

`git-word-blame` doesn't support **renames** for now and is only tested on files with a linear history.

There's also a default maxmum of `2000` commits processed. This can be removed with this command: `git config --global word-blame.limit 0`.

`WikiWho` does not yet, to my knowledge, support attributing white-space, it's attributed to the token coming after it.

## See also

 - https://github.com/wikiwho/WikiWho/ and https://github.com/wikiwho/WhoColor
   A word-by-word blame for Wikipedia with a well tested algorithm for prose (default algorithm)
   The HTML vizualisation of this project is heavily inspired by WhoColor

 - https://github.com/mediawiki-utilities/python-mwpersistence/
   An alternative authorship algorithm detection made also for Wikipedia

 - https://github.com/d33tah/wordblame
   Export Wikipedia articles to git to perform a word-by-word blame, it does that by
   putting each word in a separate line for each file in the history

 - https://github.com/lucadealfaro/authorship-tracking
   Another altenative not yet integrated

 - https://github.com/git-persistence/git-persistence
   A character-by-character authorship oriented towards scoring the authors on a whole repository
